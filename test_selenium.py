#!/usr/bin/python3
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import subprocess,os
from time import sleep
import sys


options = Options()
options.add_argument('-headless')
driver = webdriver.Firefox(service_log_path=os.devnull, options=options)
driver.get("https://vaccine3.hse.ie/s/login/SelfRegister")
element = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, "110:0")))
driver.find_element_by_id('110:0').send_keys("Son")
driver.find_element_by_id('120:0').send_keys("Goku")
driver.find_element_by_id('130:0').send_keys("son.goku@dbz.com")
driver.find_element_by_id('626:0').send_keys("999999999")
driver.find_element_by_id('643:0').send_keys(sys.argv[1])
driver.find_element_by_css_selector("button.slds-button.slds-button--neutral.sfdc_button.uiButton").click()
element = driver.find_element_by_id('error').text
try:
  assert element == 'Registration is not currently available for your age group'
except AssertionError:
  print("Registration open")
print("Registration is not currently available for your age group")
driver.quit()
